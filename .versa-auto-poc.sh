#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.10
#  Last Modified : Tue Aug  4 17:49:41 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the VERSA POC-BOT Module

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.


is_user_root() { [ "$(id -u)" -eq 0 ]; }


sh ./.header/.main-header.sh

if is_user_root; then
# echo "POCbot USER CHECK: Script is initiated by root user!                                 "
echo "                                                                                     "
else
echo "-------------------------------------------------------------------------------------"
echo "POCbot USER CHECK: Script is not initiated by root! Current User: `whoami`           "
echo "Exiting Automatically! Please run the POCbot again as root. To login as root: sudo -i"
echo "-------------------------------------------------------------------------------------"
exit
fi

echo "Enter Your choice: \c"
read template
        if [ "$template" = "1" ]
                then
                sh      ./.versa-stealthmode.sh
        elif [ "$template" = "2" ]
        then
                sh      ./.versa-autopoc.sh
        elif [ "$template" = "exit" ]
        then
                clear
                echo "--------------------------------------------------------------------------"
                echo "Have a great Day! - Versa POC-BOT                                         "
                echo "We always know automation is the future! We are always here to assist you!"
                echo "--------------------------------------------------------------------------"
        else
                echo "                                                              "
        echo "oops! Sorry, Its an invalid Entry... Please try again"
                sh ./.versa-auto-poc.sh
        fi
		
