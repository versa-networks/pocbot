#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Versa Hardware/Software Pre-check Engine module

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "========================================================================================="
echo "                           Hypervisor Hardware/Software Pre-check                        "
echo "-----------------------------------------------------------------------------------------"
echo "                                                                                         "

inxi -F

echo "QEMU/libvirt Versions:"
virsh --connect qemu:///system version | grep -v -e '^$'

echo "Validate Server KVM virtualization[If FAIL, please enable hardware virtualization in BIOS setting]"
virt-host-validate |grep "kvm\|hardware virtualization"

echo "Hardware Virtualization support: [vmx – Intel VT-x/svm – AMD SVM]"
echo "Hardware virtualization enabled CPUs count:" `egrep -c '(vmx|svm)' /proc/cpuinfo`
echo "                                                                      "
echo "Press Enter Key to continue with Existing Virtual Machine AutoSCAN    "
read wish
clear

echo "======================================================================"
echo "                 Existing Virtual Machine AutoSCAN                    "
echo "----------------------------------------------------------------------"


vm="$(mktemp)"
vmdump="$(mktemp)"
OUT="$(mktemp)"
virsh nodeinfo > $OUT
file1="$(mktemp)"
file2="$(mktemp)"

virsh list  | grep running  | awk '{ print substr($0,7) }' > $file1

sed 's/running//g' $file1 > $file2
sed 's/[ \t]*|[ \t]*/|/g;s/^[ \t]*\|[ \t]*$//g'   $file2 > $vm

# cat $vm

while read var1
do
	virsh dominfo "$var1" | grep "Name\|CPU(s):\|Max" | sort | awk '{printf $0 " "}' >> $vmdump
	echo "\n"                                                                        >> $vmdump
done < $vm

totalcpu=$(awk '{cpusum += $2} END {print cpusum}' $vmdump)
totalmem=$(awk '{memsum += $5} END {print memsum}' $vmdump)

# cat $vmdump

echo "No of CPU  Memory [KiB] VM Name"  >  $file2
echo "                                                                      "  >>  $file2

sed 's/running//g;s/Max memory://g;s/CPU(s):     //g;s/KiB Name:        /    /g' $vmdump | sort -r >> $file2

# column -t -s' '  $vmdump
cat $file2 | grep -v "^$"

echo "                                                              "
echo "----------------------------------------------------------------------"
echo "Hypervisor/Bare-Metal Server Hardware Details:"
cat $OUT | grep "CPU(s)" | awk '{print "The total Number of CPU(s)", $2 , "count"}'
cat $OUT | grep "Memory" | awk '{print "Total Physical memory:", $3 ,"[KiB]"}'
echo "                                                              "

echo "Virtual Machines currently running:"
echo The total Number of CPU allocated to running VM: $totalcpu count
echo The total amount of memory allocated to running VM: $totalmem [KiB]

rm $vm
rm $vmdump
rm $OUT
rm $file1
rm $file2
echo "                                                                      "
echo "----------------------------------------------------------------------"
echo "Press Enter Key to continue with Versa VM Interface mapping Tool      "
read wish

clear
echo "======================================================================"
echo "                Versa VM Interface mapping Tool:                      "
echo " Note:                                                                "
echo " Versa-Interface are only for VOS Boxes. Ignore if the VM is something"
echo " VOS Boxes - Controllers, CPE, Branches, Hub, Spokes                  "
echo "----------------------------------------------------------------------"
file1="$(mktemp)"
file2="$(mktemp)"
vm="$(mktemp)"
vmdump="$(mktemp)"
echo "Server-Interface VM-Interface Versa-Interface Bridge VM-Name \n"  > $vmdump

virsh list  | grep running  | awk '{ print substr($0,7) }' > $file1

sed 's/running//g' $file1 > $file2
sed 's/[ \t]*|[ \t]*/|/g;s/^[ \t]*\|[ \t]*$//g'   $file2 > $vm

while read var1
do
	virsh domiflist "$var1" | grep bridge  | awk '{print $1, " eth"NR-1 " vni-0/"NR-2,$3, "'$var1'" }'  | sed 's/vni-0\/-1/eth0/g' >> $vmdump
done < $vm
# cat $vmdump
column -t -s' ' $vmdump

rm $vm
rm $vmdump
rm $file1
rm $file2


echo "----------------------------------------------------------------------"
echo "Do you wish to go back to Main menu [y/n]: \c"
read wish
if [ "$wish" = "y" ] || [ "$wish" = "yes" ] || [ "$wish" = "Y" ] || [ "$wish" = "YES" ]
then
sh ./.versa-auto-poc.sh
elif [ "$wish" = "n" ] || [ "$wish" = "no" ] || [ "$wish" = "N" ] || [ "$wish" = "NO" ]
then
clear 
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
else
clear 
echo "--------------------------------------------------------------------------"
echo " Oops! Invalid Entry ... Exiting out of the POCbot V1.0                   "
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
fi   
		