#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Versa Management Interface Mapping module

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "==========================================================================="
echo "              Versa Management Interface Mapping Tool:                                "
echo "---------------------------------------------------------------------------"
file1="$(mktemp)"
file2="$(mktemp)"
vm="$(mktemp)"

virsh list  | grep running  | awk '{ print substr($0,7) }' > $file1
sed 's/running//g' $file1 > $file2
sed 's/[ \t]*|[ \t]*/|/g;s/^[ \t]*\|[ \t]*$//g'   $file2 | awk '{printf $1" "}' > $vm

echo " This is a tool to configure the IP address for the North bound Interfaces "
echo " of Versa Devices. It uses default credentials. Allocates IP Address by    "
echo " incrementing the 4th Octet. To have desired results, please initiate the  "
echo " script after 15 minutes of bringup the VM. Its uses VM serial console     "
echo " Note: The POCbot will reboot the VM's post IP address connfiguration      "
echo "+-------------------------------------------------------------------------+"
echo "| Different Virtual Machines up and running in your SDWAN POC Setup:      |"
echo "| [Use it for Copy and paste in the below section]                        |"
echo "+-------------------------------------------------------------------------+"
cat $vm
echo "                                                                           "
echo "---------------------------------------------------------------------------"
echo " Please enter the VM's separated by space to configure Mgmt ip on eth0     "
echo " List: \c:"
read vmlist

echo " Please enter the Starting IP Address [eg:172.30.1.2]: \c"
read startip
echo " Please enter the Subnet Mask [eg:255.255.255.0]: \c"
read mask
echo " Please enter the Gateway IP Address [eg:172.30.1.1]: \c"
read gateway

if [ -z "$startip" ] || [ -z "$mask" ] || [ -z "$gateway" ] || [ -z "$vmlist" ]
then
echo "==============================USER-ALERT=================================="
echo "   One or more user input in this section is not entered by the user      "
echo "   intentionally or unintentionally. POCbot will completely ignore the    "
echo "   data entered till now for the sake of data integrity. Please re-run    "
echo "   script                                                                 "
echo "=========================================================================="
exit
fi

baseaddr="$(echo $startip | cut -d. -f1-3)"
fourthoctet="$(echo $startip | cut -d. -f4)"

echo "==========================================================================="
echo "                     Virtual Machine Auto-SCAN Engine                      "
echo "---------------------------------------------------------------------------"
echo "                                                                           "

for var1 in $vmlist;
do 
	expect .script/.vm-console-check.exp $var1 
	status=$?
	if [ "$status" -eq "1" ] 
	then
	exit
	fi   
done

echo "---------------------------------------------------------------------------\n"
echo " Firing Configuration Engine:                                              "
echo "---------------------------------------------------------------------------\n"

for var1 in $vmlist;
do 
	TIME=$( date +'%d-%b-%Y-%H-%M-%S')
	echo " Configuration Initiated $TIME -- Device: $var1 --$baseaddr.$fourthoctet"
	expect .script/.addif.exp $var1 $baseaddr.$fourthoctet $mask $gateway  >> ./.script/.autopoc/.poctemp/logging.txt
	fourthoctet=$((fourthoctet + 1))
done

for var1 in $vmlist;
do 
	virsh destroy $var1  >> ./.script/.autopoc/.poctemp/logging.txt
done

for var1 in $vmlist;
do 
	virsh start $var1  >> ./.script/.autopoc/.poctemp/logging.txt
done

echo "---------------------------------------------------------------------------"
echo " Please manually check the ip address configured                           "
echo "---------------------------------------------------------------------------\n"

rm $vm
rm $file1
rm $file2

echo "------------------------------------------------------------- "
echo "                                                              "
echo "Do you wish to go back to Main menu [y/n]: \c"
read wish
if [ "$wish" = "y" ] || [ "$wish" = "yes" ] || [ "$wish" = "Y" ] || [ "$wish" = "YES" ]
then
sh ./.versa-auto-poc.sh
elif [ "$wish" = "n" ] || [ "$wish" = "no" ] || [ "$wish" = "N" ] || [ "$wish" = "NO" ]
then
clear 
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
else
clear 
echo "--------------------------------------------------------------------------"
echo " Oops! Invalid Entry ... Exiting out of the POCbot V1.0                   "
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
fi   
		