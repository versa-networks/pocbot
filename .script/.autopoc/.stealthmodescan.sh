#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Versa Management Interface Mapping module

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

cat /dev/null > ./.script/.autopoc/.poctemp/vbrconnect.txt
yaml1="$(mktemp)"
yaml2="$(mktemp)"

stealthmodestaticconfig() {
cat > $file <<EOF
# Auto configured with V-POCbotV1.0 -- $( date +'%d-%b-%Y-%H-%M-%S')
network:
  version: 2
  renderer: networkd
  ethernets:
    $nic:
      dhcp4: no
  bridges:
    $mgmrbr:
      addresses:
        - $staticip
      nameservers:
          addresses: [$nameserversip]
      interfaces:
        - $nic
      routes:
       - to: 0.0.0.0/0
         via: $gatewayip
         on-link: true
    vbr101:
      dhcp4: no
    vbr102:
      dhcp4: no
    vbr103:
      dhcp4: no
    vbr104:
      dhcp4: no
    vbr105:
      dhcp4: no
    vbr106:
      dhcp4: no
    vbr107:
      dhcp4: no
    vbr108:
      dhcp4: no
    vbr109:
      dhcp4: no
    vbr110:
      dhcp4: no
    vbr111:
      dhcp4: no
    vbr112:
      dhcp4: no
    vbr113:
      dhcp4: no
    vbr114:
      dhcp4: no
    vbr115:
      dhcp4: no
    vbr116:
      dhcp4: no
    vbr117:
      dhcp4: no
    vbr118:
      dhcp4: no
    vbr119:
      dhcp4: no
    vbr120:
      dhcp4: no
    vbr121:
      dhcp4: no
    vbr122:
      dhcp4: no
    vbr123:
      dhcp4: no
    vbr124:
      dhcp4: no
    vbr125:
      dhcp4: no
    vbr126:
      dhcp4: no
    vbr127:
      dhcp4: no
    vbr128:
      dhcp4: no
    vbr129:
      dhcp4: no
    vbr130:
      dhcp4: no
EOF

}

stealthmodedhcpconfig() {
cat > $file <<EOF
# Auto configured with V-POCbotV1.0 -- $( date +'%d-%b-%Y-%H-%M-%S')
network:
  version: 2
  renderer: networkd
  ethernets:
    $nic:
      dhcp4: no
  bridges:
    $mgmrbr:
      dhcp4: yes
    vbr101:
      dhcp4: no
    vbr102:
      dhcp4: no
    vbr103:
      dhcp4: no
    vbr104:
      dhcp4: no
    vbr105:
      dhcp4: no
    vbr106:
      dhcp4: no
    vbr107:
      dhcp4: no
    vbr108:
      dhcp4: no
    vbr109:
      dhcp4: no
    vbr110:
      dhcp4: no
    vbr111:
      dhcp4: no
    vbr112:
      dhcp4: no
    vbr113:
      dhcp4: no
    vbr114:
      dhcp4: no
    vbr115:
      dhcp4: no
    vbr116:
      dhcp4: no
    vbr117:
      dhcp4: no
    vbr118:
      dhcp4: no
    vbr119:
      dhcp4: no
    vbr120:
      dhcp4: no
    vbr121:
      dhcp4: no
    vbr122:
      dhcp4: no
    vbr123:
      dhcp4: no
    vbr124:
      dhcp4: no
    vbr125:
      dhcp4: no
    vbr126:
      dhcp4: no
    vbr127:
      dhcp4: no
    vbr128:
      dhcp4: no
    vbr129:
      dhcp4: no
    vbr130:
      dhcp4: no
EOF

}

controller_physical_binding_data(){
echo "Please Enter the Versa Bridge name: [Eg vbr101 ]                                       : \c"
read vbrbridgename
echo "Please Enter the Physical NIC name [ $physical ]                              : \c"
read vbrphysical

if [ -n "$vbrbridgename" ] && [ -n "$vbrphysical" ]
then
echo "$vbrbridgename $vbrphysical" >> ./.script/.autopoc/.poctemp/vbrconnect.txt
fi

echo "Do you want to add another transport bridge (y/n)                                      : \c"
read answer

if [ $answer = y ]
then controller_physical_binding_data
fi 
}

controller_physical_binding_exec(){
if [ -s ./.script/.autopoc/.poctemp/vbrconnect.txt ]
then
	while read vbrbridgename vbrphysical
	do
	sed "/ethernets.*/a \    $vbrphysical:\n      dhcp4: no" $file > $yaml2
	sed "/$vbrbridgename.*/a \      interfaces:\n        - $vbrphysical" $yaml2 > $file
	done < ./.script/.autopoc/.poctemp/vbrconnect.txt
fi
}

clear
echo "======================================================================"
echo "   To configure Hypervisor Management Bridge and Versa POC Bridges    "
echo "----------------------------------------------------------------------"
echo "Support Note:                                                         "
echo "Supported on Ubuntu 18.04 and above with Netplan network configuration"
echo "----------------------------------------------------------------------"
echo "Read with caution:                                                    "
echo "# Bot will reboot the server automatically once post config changes   "
echo "# Recommended to run this section of POCbot from Console; Else, make  "
echo "  sure an external management console is available for manual override"
echo "# Recommended to use the script at step0 for setting up the POC server"
echo "# It is expected to delete all existing network configurations and to "
echo "  create new network configurations based on user inputs              "
echo "# POCbot will autocreate 30 bridges for POC usage vbr<101-130>        "
echo "# Management Bridge will be created and Ip address will be allocated  "
echo "  to it. Server Physical interface will be part of the Bridge         "
echo "# Bot will create backup YAML file with data/time for manual recovery "
echo "  before overwriting the existing network configurations              "
echo "----------------------------------------------------------------------"
echo "Script will look for these files, and do the required config changes  " 
echo "  /etc/netplan/50-cloud-init.yaml                                     "
echo "  /etc/netplan/01-netcfg.yaml                                         "
echo "  /etc/netplan/01-network-manager-all.yaml                            "
echo "  /etc/netplan/00-installer-config.yaml                               "
echo "It is mandatory to feed-in all the inputs in this section for the bot "
echo "to initiate the interface configuration.                              "
echo "======================================================================"


is_user_root() { [ "$(id -u)" -eq 0 ]; }

if is_user_root; then
echo "POCbot USER CHECK: Script is initiated by root!"
echo "----------------------------------------------------------------------"
else
echo "POCbot USER CHECK: Script is not initiated by root!                   "
echo "Current User: `whoami`                                                "
echo "Exiting from the script Automatically! Please login as root and run   "
echo "the POCbot again. To login as root: sudo -i                           "
echo "----------------------------------------------------------------------"
exit
fi

if [ `lsb_release -a | grep Distributor | grep Ubuntu | wc -l` -ne "1" ]; then
echo "====================V-POCbotV1.0 Hypervisor SCAN======================"
echo " Identified a Non-Ubuntu operating system. Exiting from the Tool      "
echo " Please manually do the Network configuration                         "
echo " Support Note:                                                        "
echo " # Supported on Ubuntu with YAML based network configuration, Netplan "
echo " # Ubuntu Version 18.04.4 LTS (Bionic Beaver) and greater supports    "
echo "   Netplan by default.                                                "
echo "======================================================================"
exit
fi

echo "POCbot Identified Physical Interfaces in the Server:                  "
physical=`ls -l /sys/class/net | grep pci |  awk '{printf $9" "}'`
for i in $physical ; do echo "$i" ; done
echo "======================================================================"

echo "Do you wish to configure Management and POC bridges with POCbot?      "
echo "Type 'no' to discontinue, else Press Any Key to Continue            : \c"
read wish

if [ "$wish" = "no" ]
then exit
fi 

echo "Enter the name of the Management Bridge to be created [Eg: mgmtbridge]                 : \c"
read mgmrbr
echo "Enter the Physical Interface to be binded to the Management Bridge [$physical]: \c"
read nic
echo "Do you wish to configure the Management bridge with Static IP or with DHCP? "
echo "Enter 1 for Static IP and 2 for DHCP                                                   : \c"
read configure

if [ "$configure" = "1" ]
then
	echo "Enter the IP Address to be Configured to the Management Bridge [Eg: 10.192.244.29/16]  : \c"
	read staticip
	echo "Enter the Gateway IP Address to be Configured to the Management Bridge [Eg: 10.192.0.1]: \c"
	read gatewayip	
	echo "Enter the DNS/nameserver to be Configured [Eg: 8.8.8.8, 8.8.4.4]     "
	echo "IF MANY, SEPARATE BY COMMA ONLY.DONT USE TAB OR ANY OTHER OPERATOR                     : \c"
	read nameserversip

	if [ -z "$nic" ] || [ -z "$staticip" ] || [ -z "$gatewayip" ] || [ -z "$mgmrbr" ] || [ -z "$nameserversip" ]
	then
		echo "===========================USER-ALERT================================="
		echo "One or more user input in this section is not entered by the user     "
		echo "intentionally or unintentionally. POCbot will completely ignore the   "
		echo "data entered till now for the sake of data integrity. Please re-run   "
		echo "script                                                                "
		echo "======================================================================"
		exit
	fi
else
	if [ -z "$nic" ] || [ -z "$mgmrbr" ]
	then
		echo "===========================USER-ALERT================================="
		echo "One or more user input in this section is not entered by the user     "
		echo "intentionally or unintentionally. POCbot will completely ignore the   "
		echo "data entered till now for the sake of data integrity. Please re-run   "
		echo "script                                                                "
		echo "======================================================================"
		exit
	fi
	
fi 

echo "Versa POCbot will auto-create Versa POC bridges: vbr101-vbr130            "
echo "Do you wish to extend controller transport bridges to Physical NIC [y/n]               : \c"
read decision
if [ "$decision" = "y" ]
then
controller_physical_binding_data
fi

if [ -s /etc/netplan/50-cloud-init.yaml ]; then
	echo "====================V-POCbotV1.0 Hypervisor SCAN======================"
	DATE=`date +%Y%m%d%H%M`
	echo "/etc/netplan/50-cloud-init.yaml exists"
	echo "Bot is creating a backup file for manual recovery                   "
	echo "Backup file Path: /etc/netplan/50-cloud-init.yaml.bk_$DATE"
	cp /etc/netplan/50-cloud-init.yaml /etc/netplan/50-cloud-init.yaml.bk_$DATE
	echo "----------------------------------------------------------------------"
	echo "Existing /etc/netplan/50-cloud-init.yaml Content:                   "
	cat /etc/netplan/50-cloud-init.yaml
	echo "----------------------------------------------------------------------"
	file=/etc/netplan/50-cloud-init.yaml

	if [ "$configure" = "1" ]
	then
	stealthmodestaticconfig
	fi

	if [ "$configure" = "2" ]
	then
	stealthmodedhcpconfig
	fi

	controller_physical_binding_exec
	echo "VPOCbot Generated /etc/netplan/50-cloud-init.yaml Content:          "
	cat /etc/netplan/50-cloud-init.yaml
	echo "----------------------------------------------------------------------"
	lsb_release -a
fi

if [ -s /etc/netplan/01-netcfg.yaml ]; then
	echo "====================V-POCbotV1.0 Hypervisor SCAN======================"
	DATE=`date +%Y%m%d%H%M`
	echo "/etc/netplan/01-netcfg.yaml exists"
	echo "Bot is creating a backup file for manual recovery                   "
	echo "Backup file Path: /etc/netplan/01-netcfg.yaml.bk_$DATE"
	cp /etc/netplan/01-netcfg.yaml /etc/netplan/01-netcfg.yaml.bk_$DATE
	echo "----------------------------------------------------------------------"
	echo "Existing /etc/netplan/01-netcfg.yaml Content:                       "
	cat /etc/netplan/01-netcfg.yaml
	echo "----------------------------------------------------------------------"
	file=/etc/netplan/01-netcfg.yaml

	if [ "$configure" = "1" ]
	then
	stealthmodestaticconfig
	fi

	if [ "$configure" = "2" ]
	then
	stealthmodedhcpconfig
	fi

	controller_physical_binding_exec
	echo "VPOCbot Generated /etc/netplan/01-netcfg.yaml Content:              "
	cat /etc/netplan/01-netcfg.yaml
	echo "----------------------------------------------------------------------"
	lsb_release -a
fi

if [ -s /etc/netplan/01-network-manager-all.yaml ]; then
	echo "====================V-POCbotV1.0 Hypervisor SCAN======================"
	DATE=`date +%Y%m%d%H%M`
	echo "/etc/netplan/01-network-manager-all.yaml exists"
	echo "Bot is creating a backup file for manual recovery                   "
	echo "Backup file Path: /etc/netplan/01-network-manager-all.yaml.bk_$DATE "
	cp /etc/netplan/01-network-manager-all.yaml /etc/netplan/01-network-manager-all.yaml.bk_$DATE
	echo "----------------------------------------------------------------------"
	echo "Existing /etc/netplan/01-network-manager-all.yaml Content:           "
	cat /etc/netplan/01-network-manager-all.yaml
	echo "----------------------------------------------------------------------"
	file=/etc/netplan/01-network-manager-all.yaml

	if [ "$configure" = "1" ]
	then
	stealthmodestaticconfig
	fi

	if [ "$configure" = "2" ]
	then
	stealthmodedhcpconfig
	fi

	controller_physical_binding_exec
	echo "VPOCbot Generated /etc/netplan/01-network-manager-all.yaml Content:  "
	cat /etc/netplan/01-network-manager-all.yaml
	echo "----------------------------------------------------------------------"
	lsb_release -a
fi

if [ -s /etc/netplan/00-installer-config.yaml ]; then
	echo "====================V-POCbotV1.0 Hypervisor SCAN======================"
	DATE=`date +%Y%m%d%H%M`
	echo "/etc/netplan/00-installer-config.yaml exists"
	echo "Bot is creating a backup file for manual recovery                     "
	echo "Backup file Path: /etc/netplan/00-installer-config.yaml.bk_$DATE    "
	cp /etc/netplan/00-installer-config.yaml /etc/netplan/00-installer-config.yaml.bk_$DATE
	echo "----------------------------------------------------------------------"
	echo "Existing /etc/netplan/00-installer-config.yaml Content:               "
	cat /etc/netplan/00-installer-config.yaml
	echo "----------------------------------------------------------------------"
	file=/etc/netplan/00-installer-config.yaml

	if [ "$configure" = "1" ]
	then
	stealthmodestaticconfig
	fi

	if [ "$configure" = "2" ]
	then
	stealthmodedhcpconfig
	fi

	controller_physical_binding_exec
	echo "VPOCbot Generated /etc/netplan/00-installer-config.yaml Content:      "
	cat /etc/netplan/00-installer-config.yaml
	echo "----------------------------------------------------------------------"
	lsb_release -a
fi

echo "=========================================================================="
echo "Note: POCbot is applying the Network configuration changes via NETPLAN    "
echo "=========================================================================="
echo "Please wait. ... POCbot is Initiating a Standard Reboot to the Server     "
echo "Momentarily, it is expected to lose [ssh/telnet] access to the device     "
echo "Please login via console to see the boot sequence.                        "
echo "We always know automation is the future! We are always here to assist you!"
echo "=========================================================================="
sudo netplan apply                                                          
sleep 5                                                                    
reboot                                                                      
echo "=========================================================================="