#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the VERSA POC-BOT Module 

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "------------------------------------------------------------------------ "
echo "Part 2:                                                                  \n"
echo " Please be handy with Bridge related information of your Network Topology"
echo " connecting Versa Director, Analytics, Controller/CPE/Branches           \n"
echo " Bridges will start with the naming terminology vbr101 to vbr130         " 
echo "------------------------------------------------------------------------ "
echo "Are we good to start? [Press Enter Key once you are Ready! \c"
read dummy
echo "\n"
echo "Do you wish to add the North Bound Mgmt interfaces of all desired VM to"
echo "an existing Management Bridge? [y/n]: \c"
read tagmgmt

cat /dev/null > ./.script/.autopoc/.poctemp/data_interface_build1.txt
cat /dev/null > ./.script/.autopoc/.poctemp/data_interface_build2.txt
	if [ "$tagmgmt" = "y" ]
	then
	clear
	echo "Note: POC-BOT will create the mgmt bridge with the specified name for    "
	echo "workflow redundancy                                                      "
	echo "------------------------------------------------------------------------ "
	echo "Please enter the Mgmt bridge name [eg: br0]: \c"
	read mgmtbridge
	echo "\n"
	echo "Running command -- brctl addbr $mgmtbridge"
	sudo brctl addbr $mgmtbridge
	echo "Running command -- ifconfig $mgmtbridge up"
	sudo ifconfig $mgmtbridge up
	echo "+----------------------------------------------+"
	echo "|Different Components of your SDWAN POC Setup: |"
	echo "|Use it for Copy and paste in the below section|"
	echo "+----------------------------------------------+"
	cat ./.script/.autopoc/.poctemp/masterdata.txt | awk '{printf  $2" " }'
	echo "                                            "
	echo "Enter the devices to be part of mgmt bridge -- $mgmtbridge:"
	read devicelists
		for device in $devicelists; do
		echo "mark$device --network=bridge:$mgmtbridge,model=virtio " >> ./.script/.autopoc/.poctemp/data_interface_build1.txt
		done
	fi   

clear
echo "-------------------------------------------------------------------------"
echo "Topology Bridge Binding Tool: [starts from vbr101]"
if [ -z "$mgmtbridge" ]
then
echo "You have not entered a pre-configured Mgmt bridge, Please use vbr101 as  "
echo "your Mgmt Bridge to map eth0 of all Versa VM's to the Mgmt Bridge        "
else
echo "You have entered bridge $mgmtbridge as your Mgmt bridge. Preferably use  "
echo "vbr101 as your Transport Bridge for the ease of Template Management      "
fi
echo "-------------------------------------------------------------------------"
echo "Enter the number of bridges in your topology: \c"
read bcount
echo "Creating Bridges:"
echo "-------------------------------------------------------------------------"
echo "Note:                                                                    "
echo "POC-BOT will attempt to create the bridges for workflow redundancy       "
echo "It is preferable to pre-configure the hypervisor with bridges vbr101-130 "
echo "-------------------------------------------------------------------------"

brimax=0
vbr=101
while [ $brimax -lt $bcount ];
do
	echo "Running command brctl addbr vbr$vbr"
	sudo brctl addbr vbr$vbr
	echo "Running command ifconfig vbr$vbr up"
	sudo ifconfig vbr$vbr up
brimax=$((brimax + 1))
vbr=$((vbr + 1))
done

echo "+----------------------------------------------+"
echo "|Different Components of your SDWAN POC Setup: |"
echo "|Use it for Copy and paste in the below section|"
echo "+----------------------------------------------+"
cat ./.script/.autopoc/.poctemp/masterdata.txt | awk '{printf  $2" " }'
echo "                                            "

brimax=0
vbr=101
while [ $brimax -lt $bcount ];
do
    echo "Enter the devices connected to vbr$vbr with space:"
	read devicelists
	    for device in $devicelists; do
		echo "mark$device --network=bridge:vbr$vbr,model=virtio " >> ./.script/.autopoc/.poctemp/data_interface_build2.txt
		done
brimax=$((brimax + 1))
vbr=$((vbr + 1))
done

clear
echo "===================================================================="
echo " FIRE-AND-FORGET ENGINE INITIATED!                                  "
sh ./.script/.autopoc/.autopocsession3.sh
