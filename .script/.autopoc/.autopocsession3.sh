#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the VERSA POC-BOT Module 

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

cat /dev/null > .script/.autopoc/.poctemp/virshdata1.txt

STARTTIME=$( date +'%d-%b-%Y-%H-%M-%S')
echo "===================================================================="
echo "Versa POC-BOT Image Processor: Initiating Image Forging....         "
echo "POC-BOT POC Creation Starting Time: $STARTTIME                      "
echo "===================================================================="

while read var1 devicename path var4 var5
do
	if test -f "$path"; then
		file=$(basename "$path")
		dir=$(dirname "$path")
		newfile=$dir\/$devicename-$file
		echo "Forging file $path -> $newfile"
 		cp $path  $newfile
		echo "$devicename $path $var4 $var5 $newfile" >> .script/.autopoc/.poctemp/virshdata1.txt
		var1=$(stat -c %s $path)
		var2=$(stat -c %s $newfile)
		
		if [ "$var1" -lt "$var2" ]; then
			echo 'File Replication Failure! Stopping the script!'
			exit
		elif [ "$var1" -gt "$var2" ]; then
			echo 'File Replication Failure! Stopping the script!'
			exit
		elif [ "$var1" -eq "$var2" ]; then
			echo 'Verfied the File size of source and destination File! All Good'
		fi
	else
		echo "===================================================================="
		echo "Versa Image Processor could not Identify the file: $path"
		echo "Please run the script again with the right path"
		echo 'File Replication Failure! Stopping the script!'
		echo "===================================================================="
		exit
	fi
done < .script/.autopoc/.poctemp/masterdata.txt

sh ./.script/.autopoc/.autopocsession4.sh $STARTTIME
