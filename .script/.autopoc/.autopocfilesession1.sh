#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the VERSA POC-BOT Module 

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "This module will build dynamic SDWAN POC VM Infra based on userdata entered in masterdata.txt file"
echo "Are we good to move to start? [yes/no]:\c"

read readiness
	if [ "$readiness" = "no" ]
	then
	exit
	elif [ "$readiness" = "NO" ]
	then
	exit
	fi   
clear

cat /dev/null > ./.script/.autopoc/.poctemp/masterdata.txt

     dimage=`cat masterdata.txt | grep 'Versa Director Full Image Path'                  | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
     dnames=`cat masterdata.txt | grep 'Versa Director instance names'                   | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
       dcpu=`cat masterdata.txt | grep 'Versa Director CPU counts'                       | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
       dmem=`cat masterdata.txt | grep 'Versa Director Memory in Mb'                     | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
     aimage=`cat masterdata.txt | grep 'Versa Analytics Full Image Path'                 | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
     anames=`cat masterdata.txt | grep 'Versa Analytics instance names'                  | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
       acpu=`cat masterdata.txt | grep 'Versa Analytics CPU counts'                      | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
       amem=`cat masterdata.txt | grep 'Versa Analytics Memory in Mb'                    | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
     cimage=`cat masterdata.txt | grep 'Versa Controller Full Image Path'                | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
     cnames=`cat masterdata.txt | grep 'Versa Controller instance names'                 | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
       ccpu=`cat masterdata.txt | grep 'Versa Controller CPU counts'                     | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
       cmem=`cat masterdata.txt | grep 'Versa Controller Memory in Mb'                   | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
   cpenames=`cat masterdata.txt | grep 'Versa CPE-Branches instance names'               | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
     cpecpu=`cat masterdata.txt | grep 'Versa CPE-Branches CPU counts'                   | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
     cpemem=`cat masterdata.txt | grep 'Versa CPE-Branches Memory in Mb'                 | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
 mgmtbridge=`cat masterdata.txt | grep 'Existing Management Bridge Name'                 | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`
devicelists=`cat masterdata.txt | grep 'Instance Names to be part of Management Bridge'  | sed 's/^[^:]*:/:/' | sed 's/://g' |  awk '{$1=$1};1'`

if [ -z "$mgmtbridge" ]
then
	echo "No Management Bridge Name given exclusivelly"
else
	check_mgmt=`brctl showmacs "$mgmtbridge" | grep port | wc -l`
	if [ $check_mgmt = "0" ]
	then
	clear
    echo "============================================================"
	echo "Bridge with Name $mgmtbridge is not configured in the system"
	echo "Exiting the POCbot script. Please configure the bridge with "
	echo "name $mgmtbridge manually or with the help of POCbot        "
	echo "# sh v-POCbot                                               "
	echo "   └── 1 Preparing the Server                               "
	echo "       └── 1 - Configure Server Management and POC Bridges  "
    echo "============================================================"
	exit
	fi
fi

if [ -n "$dimage" ] 
then
	qcowfile=`file $dimage | grep "QEMU QCOW" | wc -l`
	if [ $qcowfile -eq 0 ]; then
		echo "==================================================================================="
		echo "Versa Image Processor could not Identify a valid QEMU QCOW Image in the below path "
		echo "$dimage                                           "
		echo "Please initiate the script again with the right path! Stopping the script!         "
		echo "==================================================================================="
		exit
	fi
fi

if [ -z "$dnames" ] || [ -z "$dimage" ] || [ -z "$dcpu" ] || [ -z "$dmem" ]
then
	echo "=========================USER-ALERT================================="
	echo "Versa Director will not be initiated by the Versa Quick-POC Engine  "
	echo "One or more user input for Versa Director is not entered by the"
	echo "user intentionally or unintentionally. POCbot will completely ignore"
	echo "this sub-section ONLY for the sake of data integrity.               "
	echo "===================================================================="
else
	for i in $dnames ; do echo "Director $i $dimage $dcpu $dmem"   >> ./.script/.autopoc/.poctemp/masterdata.txt   ; done
fi

if [ -n "$aimage" ] 
then
	qcowfile=`file $aimage | grep "QEMU QCOW" | wc -l`
	if [ $qcowfile -eq 0 ]; then
		echo "==================================================================================="
		echo "Versa Image Processor could not Identify a valid QEMU QCOW Image in the below path "
		echo "$aimage                                           "
		echo "Please initiate the script again with the right path! Stopping the script!         "
		echo "==================================================================================="
		exit
	fi
fi


if [ -z "$anames" ] || [ -z "$aimage" ] || [ -z "$acpu" ] || [ -z "$amem" ]
then
	echo "=========================USER-ALERT================================="
	echo "Versa Analytics will not be initiated by the Versa Quick-POC Engine "
	echo "One or more user input in this for Versa Analytics is not entered by the"
	echo "user intentionally or unintentionally. POCbot will completely ignore"
	echo "this sub-section ONLY for the sake of data integrity.               "
	echo "===================================================================="
else
	for i in $anames ; do echo "Analytics $i $aimage $acpu $amem"   >> ./.script/.autopoc/.poctemp/masterdata.txt   ; done
fi

if [ -n "$cimage" ] 
then
	qcowfile=`file $cimage | grep "QEMU QCOW" | wc -l`
	if [ $qcowfile -eq 0 ]; then
		echo "==================================================================================="
		echo "Versa Image Processor could not Identify a valid QEMU QCOW Image in the below path "
		echo "$cimage                                           "
		echo "Please initiate the script again with the right path! Stopping the script!         "
		echo "==================================================================================="
		exit
	fi
fi

if [ -z "$cnames" ] || [ -z "$cimage" ] || [ -z "$ccpu" ] || [ -z "$cmem" ]
then
	echo "=========================USER-ALERT================================="
	echo "Versa Controller will not be initiated by the Versa Quick-POC Engine"
	echo "One or more user input in this for Controller is not entered by the"
	echo "user intentionally or unintentionally. POCbot will completely ignore"
	echo "this sub-section ONLY for the sake of data integrity.               "
	echo "===================================================================="
else
	for i in $cnames ; do echo "Controller $i $cimage $ccpu $cmem"   >> ./.script/.autopoc/.poctemp/masterdata.txt   ; done
fi

if [ -z "$cpenames" ] || [ -z "$cimage" ] || [ -z "$cpecpu" ] || [ -z "$cpemem" ]
then
	echo "=========================USER-ALERT================================="
	echo "Versa VOS CPE will not be initiated by the Versa Quick-POC Engine   "
	echo "One or more user input in this for CPE is not entered by the"
	echo "user intentionally or unintentionally. POCbot will completely ignore"
	echo "this sub-section ONLY for the sake of data integrity.               "
	echo "===================================================================="
else
	for i in $cpenames ; do echo "CPE $i $cimage $cpecpu $cpemem"   >> ./.script/.autopoc/.poctemp/masterdata.txt   ; done
fi

if [ -s ./.script/.autopoc/.poctemp/masterdata.txt ]
then
	echo "==================================================================================================="
	echo "                    Data extracted from masterdata.txt to the Versa POCbot Engine                  "
	echo "==================================================================================================="
	column -t -s' '   ./.script/.autopoc/.poctemp/masterdata.txt
	echo "==================================================================================================="
else
	echo "=========================USER-ALERT=================================="
	echo "SORRY! Stopping Versa QUICK SDWAN POC Installation Engine!-POCbotV1.0"
	echo "No valid user input shared in all 3 sub-sections 1.1, 1.2 and 1.3 by "
	echo "the user intentionally or unintentionally.                           "
	echo "Please read the GUIDANCE NOTE instructions carefully on initiating   " 
	echo "this module. Thank you using Versa POCbotV1.0                        "
	echo "====================================================================="
	exit
fi

echo "Are we good to move to the next Part? \n"
echo " Type 'yes' or any other key to move to next part"
echo " Type 'no' to edit the masterdata.txt file and re-run it"
echo "[yes/no]:\c"

read readiness
	if [ "$readiness" = "no" ]
	then
	exit
	elif [ "$readiness" = "NO" ]
	then
	exit
	fi   
clear
# sh ./.script/.autopoc/.autopocsession2.sh

cat /dev/null > ./.script/.autopoc/.poctemp/data_interface_build1.txt
cat /dev/null > ./.script/.autopoc/.poctemp/data_interface_build2.txt
if [ -z "$mgmtbridge" ]
then
	echo "No Management Bridge Name given exclusivelly"
else
		for device in $devicelists; do
		echo "mark$device --network=bridge:$mgmtbridge,model=virtio " >> ./.script/.autopoc/.poctemp/data_interface_build1.txt
		done
fi

bcount=30
brimax=0
vbr=101
while [ $brimax -lt $bcount ];
do
		devicelists=`cat masterdata.txt | grep BRIDGE-vbr$vbr | awk '{$1=$2=""; print $0}'  |  awk '{$1=$1};1')`
	    for device in $devicelists; do
		echo "mark$device --network=bridge:vbr$vbr,model=virtio " >> ./.script/.autopoc/.poctemp/data_interface_build2.txt
		done
brimax=$((brimax + 1))
vbr=$((vbr + 1))
done

clear
echo "===================================================================="
echo " FIRE-AND-FORGET ENGINE INITIATED!                                  "
sh ./.script/.autopoc/.autopocsession3.sh
