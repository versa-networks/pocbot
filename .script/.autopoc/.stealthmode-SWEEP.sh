#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Versa Management Interface Mapping module

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "==========================================================================="
echo "             S.W.E.E.P - A.L.L   [Use with DOUBLE CAUTION!]                "
echo "---------------------------------------------------------------------------"
echo "Read with caution:                                                         "
echo " This Engine is used to permanently delete all the Virtual Machines and the"
echo " associated storage volumes in the Hypervisor/Bare Metal Server.           "
echo " Steps Carried out:                                                        "
echo " i.  Force-stopping of all virtual machines                                "
echo " ii. Deleting all Virtual Machines and its associated storage volumes      "
file1="$(mktemp)"
file2="$(mktemp)"
vm="$(mktemp)"

virsh list --all | grep -v " Id    Name    " | grep -v "\---------------------------" | awk '{ print substr($0,7) }' > $file1
sed 's/running//g' $file1 > $file2
sed 's/[ \t]*|[ \t]*/|/g;s/^[ \t]*\|[ \t]*$//g'   $file2 | awk '{printf $1" "}' > $vm

echo "+-------------------------------------------------------------------------+"
echo "| List of Virtual Machines in your SDWAN POC Setup which will be deleted: |"
echo "+-------------------------------------------------------------------------+"
cat $vm
echo "                                                                           "
echo "-----NO WAY BACK!---ALL VM'S WILL BE PERMANENTLY DELETED WITH 'ok'!--------"
echo "Type 'ok' and press ENTER KEY to permanently delete all the Virtual Machines!"
echo "Type any other KEY to stop this!                                           "
echo "User Input: \c:"
read data

allvm=`cat $vm`
if [ "$data" = "ok" ]; then
	for var1 in $allvm;
	do 
		TIME=$( date +'%d-%b-%Y-%H-%M-%S')
		echo "$TIME -- Force-stopping of virtual machine $var1"
		virsh destroy $var1
	done
	for var1 in $allvm;
	do 
		TIME=$( date +'%d-%b-%Y-%H-%M-%S')
		echo "$TIME -- Deleting the Virtual Machine $var1 and all its associated storage volumes"
		virsh undefine --remove-all-storage $var1
	done
fi

rm $vm
rm $file1
rm $file2

echo "+-------------------------------------------------------------------------+"
echo "                                                                           "
echo "Do you wish to go back to Main menu [y/n]: \c"
read wish
if [ "$wish" = "y" ] || [ "$wish" = "yes" ] || [ "$wish" = "Y" ] || [ "$wish" = "YES" ]
then
sh ./.versa-auto-poc.sh
elif [ "$wish" = "n" ] || [ "$wish" = "no" ] || [ "$wish" = "N" ] || [ "$wish" = "NO" ]
then
clear 
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
else
clear 
echo "--------------------------------------------------------------------------"
echo " Oops! Invalid Entry ... Exiting out of the POCbot V1.0                   "
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
fi   
		