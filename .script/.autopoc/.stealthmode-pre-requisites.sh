clear
echo "======================================================================"
echo "       Install PRE-REQUISITES packages for POCbotv1.0 in Server       "
echo "----------------------------------------------------------------------"
echo "Support Note:                                                         "
echo "# Supported on Ubuntu operating system ONLY                           "
echo "# Works well with Ubuntu Version 18.04 LTS (Bionic Beaver)            "
echo "# Go through the README document before you begin the installation    "
echo "# Please run the POCbot Tool as root User                             "
echo "----------------------------------------------------------------------"

is_user_root() { [ "$(id -u)" -eq 0 ]; }

if is_user_root; then
    echo "POCbot USER CHECK: Script is initiated by root!                       "
	echo "----------------------------------------------------------------------"
else
	echo "POCbot USER CHECK: Script is not initiated by root!                   "
	echo "Current User: `whoami`                                                "
	echo "Exiting from the script Automatically! Please login as root and run   "
	echo "the POCbot again. To login as root: sudo -i                           "
	echo "----------------------------------------------------------------------"
    exit
fi

echo "----------------------------------------------------------------------"
echo "Do you wish POCbot to Install the PRE-REQUISITES packages?            "
echo "Type 'no' to exit or any other key to proceed with the installation: \c"
read wish
if [ "$wish" = "no" ]
then
clear 
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
fi   

TIME=$( date +'%d-%b-%Y-%H-%M-%S')
echo "`date +'%d-%b-%Y-%H-%M-%S'`: Started Installing Packages" >> .script/.autopoc/.poctemp/logging.txt
echo "`date +'%d-%b-%Y-%H-%M-%S'`: Started Installing Packages"
apt-get -y update                                            
apt-get -y upgrade                                           
apt-get -f -y install qemu-kvm 
apt-get -f -y install libvirt-bin 
apt-get -f -y install virt-manager
apt-get -f -y install sshpass                                    
apt-get -f -y install bridge-utils                               
apt-get -f -y install libvirt-clients                            
apt-get -f -y install virtinst                                   
apt-get -f -y install inxi                                       
apt-get -f -y install expect
apt-get -f -y install net-tools
apt-get update                                            
apt-get upgrade                                           
echo "`date +'%d-%b-%Y-%H-%M-%S'`: Completed Installing Packages"
echo "`date +'%d-%b-%Y-%H-%M-%S'`: Completed Installing Packages" >> .script/.autopoc/.poctemp/logging.txt
chmod -R 777 /var/lib/libvirt/images

echo "------------------------------------------------------------- "
echo "                                                              "
echo "Do you wish to go back to Main menu [y/n]: \c"
read wish
if [ "$wish" = "y" ] || [ "$wish" = "yes" ] || [ "$wish" = "Y" ] || [ "$wish" = "YES" ]
then
sh ./.versa-auto-poc.sh
elif [ "$wish" = "n" ] || [ "$wish" = "no" ] || [ "$wish" = "N" ] || [ "$wish" = "NO" ]
then
clear 
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
else
clear 
echo "--------------------------------------------------------------------------"
echo " Oops! Invalid Entry ... Exiting out of the POCbot V1.0                   "
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
fi   
