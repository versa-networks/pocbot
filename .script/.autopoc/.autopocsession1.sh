#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the VERSA POC-BOT Module 

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "========================================================================="
echo "           Versa QUICK SDWAN POC Installation BOT GUIDANCE NOTE          "
echo "-------------------------------------------------------------------------"
echo "POCbot gather user data in two parts [1/2] as below,                     "
echo "Part 1: Used to gather POC VM specification Information                  "
echo "        1.1: Versa Director specific details                             "
echo "        1.2: Versa Analytics specific details                            "
echo "        1.3: Versa Controller/CPE/Branches specific details              "
echo "Part 2: Used to gather Bridge/Interface Information                      "
echo "-------------------------------------------------------------------------"
echo "Note: At any point of time, you may terminate the bot by pressing Ctrl+C "
echo "-------------------------------------------------------------------------"
echo "Are we good to start? [Press ENTER Key once you are Ready! \c"
read dummy
cat /dev/null > ./.script/.autopoc/.poctemp/masterdata.txt

clear
echo "Section 1.1: Please enter Versa Director specific details:"
echo "==========================================================\n"
echo "Image location [Eg: /var/lib/libvirt/images/16.1R2S9/versa-director-a454c1d-16.1R2S9.qcow2]:"
echo "[Full File Path]: \c"
read dimage

if [ -n "$dimage" ] 
then
	qcowfile=`file $dimage | grep "QEMU QCOW" | wc -l`
	if [ $qcowfile -eq 0 ]; then
		echo "==================================================================================="
		echo "Versa Image Processor could not Identify a valid QEMU QCOW Image in the below path "
		echo "$dimage                                           "
		echo "Please initiate the script again with the right path! Stopping the script!         "
		echo "==================================================================================="
		exit
	fi
fi

echo "VM names [separated by spaces]: \c"
read dnames
echo "CPU counts per VM: \c"
read dcpu
echo "Memory in Mb:"
echo "   1 Gb - 1024 Mb           2 Gb - 2048 Mb          4 Gb - 4096 Mb       "
echo "   6 Gb - 6144 Mb           8 Gb - 8192 Mb         10 Gb - 10240 Mb      "
echo "[Eg: For 8 GB Enter 8192]: \c"
read dmem

if [ -z "$dnames" ] || [ -z "$dimage" ] || [ -z "$dcpu" ] || [ -z "$dmem" ]
then
	echo "=========================USER-ALERT================================="
	echo "Versa Director will not be initiated by the Versa Quick-POC Engine  "
	echo "One or more user input in this sub-section 1.1 is not entered by the"
	echo "user intentionally or unintentionally. POCbot will completely ignore"
	echo "this sub-section ONLY for the sake of data integrity.               "
	echo "Press ENTER Key to continue with the next sections!                 "
	echo "===================================================================="
	read dummy
else
	for i in $dnames ; do echo "Director $i $dimage $dcpu $dmem"   >> ./.script/.autopoc/.poctemp/masterdata.txt   ; done
fi

clear
echo "Section 1.2: Please enter Versa Analytics specific details:"
echo "===========================================================\n"
echo "Image location [Eg: /var/lib/libvirt/images/16.1R2S9/versa-analytics-80561de-16.1R2S9.qcow2]:"
echo "[Full File Path]: \c"
read aimage

if [ -n "$aimage" ] 
then
	qcowfile=`file $aimage | grep "QEMU QCOW" | wc -l`
	if [ $qcowfile -eq 0 ]; then
		echo "==================================================================================="
		echo "Versa Image Processor could not Identify a valid QEMU QCOW Image in the below path "
		echo "$aimage                                           "
		echo "Please initiate the script again with the right path! Stopping the script!         "
		echo "==================================================================================="
		exit
	fi
fi

echo "VM names [separated by spaces]: \c"
read anames
echo "CPU counts per VM: \c"
read acpu
echo "Memory in Mb:"
echo "   1 Gb - 1024 Mb           2 Gb - 2048 Mb          4 Gb - 4096 Mb       "
echo "   6 Gb - 6144 Mb           8 Gb - 8192 Mb         10 Gb - 10240 Mb      "
echo "[Eg: For 8 GB Enter 8192]: \c"
read amem

if [ -z "$anames" ] || [ -z "$aimage" ] || [ -z "$acpu" ] || [ -z "$amem" ]
then
	echo "=========================USER-ALERT================================="
	echo "Versa Analytics will not be initiated by the Versa Quick-POC Engine "
	echo "One or more user input in this sub-section 1.2 is not entered by the"
	echo "user intentionally or unintentionally. POCbot will completely ignore"
	echo "this sub-section ONLY for the sake of data integrity.               "
	echo "Press ENTER Key to continue with the next sections!                 "
	echo "===================================================================="
	read dummy
else
	for i in $anames ; do echo "Analytics $i $aimage $acpu $amem"   >> ./.script/.autopoc/.poctemp/masterdata.txt   ; done
fi

clear
echo "Section 1.3: Please enter Versa Controller/CPE/Branches specific details:"
echo "=========================================================================\n"
echo "Image location [Eg: /var/lib/libvirt/images/16.1R2S9/versa-flexvnf-80561de-16.1R2S9.qcow2]:"
echo "[Full File Path]: \c"
read cimage

if [ -n "$cimage" ] 
then
	qcowfile=`file $cimage | grep "QEMU QCOW" | wc -l`
	if [ $qcowfile -eq 0 ]; then
		echo "==================================================================================="
		echo "Versa Image Processor could not Identify a valid QEMU QCOW Image in the below path "
		echo "$cimage                                           "
		echo "Please initiate the script again with the right path! Stopping the script!         "
		echo "==================================================================================="
		exit
	fi
fi

echo "Controller spec can be different from CPE/Branches!\n"

echo "Controller Specific Date:"
echo "-------------------------"
echo "Controller VM names [separated by spaces]: \c"
read cnames
echo "CPU counts per controller VM: \c"
read ccpu
echo "Memory in Mb:"
echo "   1 Gb - 1024 Mb           2 Gb - 2048 Mb          4 Gb - 4096 Mb       "
echo "   6 Gb - 6144 Mb           8 Gb - 8192 Mb         10 Gb - 10240 Mb      "
echo "[Eg: For 8 GB Enter 8192]: \c"
read cmem

echo "VOS CPE Specific Date:"
echo "----------------------"
echo "CPE/Branches VM names [separated by spaces]: \c"
read cpenames
echo "CPU counts per CPE/Branches VM: \c"
read cpecpu
echo "Memory in Mb:"
echo "   1 Gb - 1024 Mb           2 Gb - 2048 Mb          4 Gb - 4096 Mb       "
echo "   6 Gb - 6144 Mb           8 Gb - 8192 Mb         10 Gb - 10240 Mb      "
echo "[Eg: For 8 GB Enter 8192]: \c"
read cpemem

if [ -z "$cnames" ] || [ -z "$cimage" ] || [ -z "$ccpu" ] || [ -z "$cmem" ]
then
	echo "=========================USER-ALERT================================="
	echo "Versa Controller will not be initiated by the Versa Quick-POC Engine"
	echo "One or more user input in this sub-section 1.3 is not entered by the"
	echo "user intentionally or unintentionally. POCbot will completely ignore"
	echo "this sub-section ONLY for the sake of data integrity.               "
	echo "Press ENTER Key to continue with the next sections!                 "
	echo "===================================================================="
	read dummy
else
	for i in $cnames ; do echo "Controller $i $cimage $ccpu $cmem"   >> ./.script/.autopoc/.poctemp/masterdata.txt   ; done
fi

if [ -z "$cpenames" ] || [ -z "$cimage" ] || [ -z "$cpecpu" ] || [ -z "$cpemem" ]
then
	echo "=========================USER-ALERT================================="
	echo "Versa VOS CPE will not be initiated by the Versa Quick-POC Engine   "
	echo "One or more user input in this sub-section 1.3 is not entered by the"
	echo "user intentionally or unintentionally. POCbot will completely ignore"
	echo "this sub-section ONLY for the sake of data integrity.               "
	echo "Press ENTER Key to continue with the next sections!                 "
	echo "===================================================================="
	read dummy
else
	for i in $cpenames ; do echo "CPE $i $cimage $cpecpu $cpemem"   >> ./.script/.autopoc/.poctemp/masterdata.txt   ; done
fi

clear

if [ -s ./.script/.autopoc/.poctemp/masterdata.txt ]
then
	echo "====================================================================="
	echo "         Master Data Feeded to the Versa POC-BOT Engine              "
	echo "====================================================================="
	column -t -s' '   ./.script/.autopoc/.poctemp/masterdata.txt
	echo "====================================================================="
else
	echo "=========================USER-ALERT=================================="
	echo "SORRY! Stopping Versa QUICK SDWAN POC Installation Engine!-POCbotV1.0"
	echo "No valid user input shared in all 3 sub-sections 1.1, 1.2 and 1.3 by "
	echo "the user intentionally or unintentionally.                           "
	echo "Please read the GUIDANCE NOTE instructions carefully on initiating   " 
	echo "this module. Thank you using Versa POCbotV1.0                        "
	echo "====================================================================="
	exit
fi

echo "Are we good to move to the next Part? \n"
echo " Type 'yes' or any other key to move to next part"
echo " Type 'no' to erase the data as part of this Section and re-enter it"
echo "[yes/no]:\c"

read readiness
	if [ "$readiness" = "no" ]
	then
	sh ./.script/.autopoc/.autopocsession1.sh
	elif [ "$readiness" = "NO" ]
	then
	sh ./.script/.autopoc/.autopocsession1.sh
	fi   
clear
sh ./.script/.autopoc/.autopocsession2.sh
