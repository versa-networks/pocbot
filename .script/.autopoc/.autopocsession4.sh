#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the VERSA POC-BOT Module

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear

startVM="$(mktemp)"
cat /dev/null > .script/.autopoc/.poctemp/virshdata0.txt
cat /dev/null > .script/.autopoc/.poctemp/virshdata2.txt
cat /dev/null > .script/.autopoc/.poctemp/virshdata3.txt
cat /dev/null > .script/.autopoc/.poctemp/virshdata4.txt
cat /dev/null > .script/.autopoc/.poctemp/data_interface_build3.txt
STARTTIME=$1

echo "==========================================================================="
TIME=$( date +'%d-%b-%Y-%H-%M-%S')
echo " Building the Virtual Machines: $TIME                                      "
echo "---------------------------------------------------------------------------"

while read devicename var2 cpu memory path
do

echo "mark$devicename virt-install --connect qemu:///system --name $devicename --ram $memory --vcpus $cpu --cpu SandyBridge --disk path=$path,format=qcow2,bus=virtio,cache=none --vnc --os-type=linux --os-variant=debian8 --accelerate --noautoconsole --keymap=en-us --import --autostart "  >>.script/.autopoc/.poctemp/virshdata2.txt
echo "mark$devicename  $var2 $cpu $memory $path $devicename" >>.script/.autopoc/.poctemp/virshdata0.txt

done < .script/.autopoc/.poctemp/virshdata1.txt

while read devicename var2 cpu memory path
do

cat .script/.autopoc/.poctemp/data_interface_build1.txt  .script/.autopoc/.poctemp/virshdata2.txt | sort -r | grep $devicename | awk '{printf $0}' >> .script/.autopoc/.poctemp/virshdata3.txt
echo " " >> .script/.autopoc/.poctemp/virshdata3.txt
done < .script/.autopoc/.poctemp/virshdata0.txt

while read devicename var2 cpu memory path actualdevicename
do

cat .script/.autopoc/.poctemp/data_interface_build2.txt | grep $devicename | sort    | awk '{printf $0}' >> .script/.autopoc/.poctemp/data_interface_build3.txt
echo " "  >> .script/.autopoc/.poctemp/data_interface_build3.txt
cat .script/.autopoc/.poctemp/data_interface_build3.txt  .script/.autopoc/.poctemp/virshdata3.txt | sort -r | grep $devicename | awk '{printf $0}' > startVM
sed -i "s/$devicename//g" startVM
TIME=$( date +'%d-%b-%Y-%H-%M-%S')
echo " $TIME -- Firing VM: $actualdevicename"

sh startVM
sleep 60
cat .script/.autopoc/.poctemp/data_interface_build3.txt  .script/.autopoc/.poctemp/virshdata3.txt | sort -r | grep $devicename | awk '{printf $0}' >> .script/.autopoc/.poctemp/virshdata4.txt
echo " " >> .script/.autopoc/.poctemp/virshdata4.txt
sed -i "s/$devicename//g" .script/.autopoc/.poctemp/virshdata4.txt
done < .script/.autopoc/.poctemp/virshdata0.txt

rm startVM
clear
echo "==========================================================================="
echo " Your SDWAN POC Infra SETUP is Ready! Thank you for Using Versa POC-BOT!   "
FINISHTIME=$( date +'%d-%b-%Y-%H-%M-%S')
echo " Time taken to create the POC Infra Setup:                                 "
echo " START TIME: $STARTTIME FINISH TIME: $FINISHTIME                           "
echo "---------------------------------------------------------------------------"
echo " List of VM's installed in your setup!                                     "
virsh list --all
echo "---------------------------------------------------------------------------"
sh ./.script/.autopoc/.stft
echo "Do you wish to go back to Main menu [y/n]: \c"
read wish
if [ "$wish" = "y" ] || [ "$wish" = "yes" ] || [ "$wish" = "Y" ] || [ "$wish" = "YES" ]
then
sh ./.versa-auto-poc.sh
elif [ "$wish" = "n" ] || [ "$wish" = "no" ] || [ "$wish" = "N" ] || [ "$wish" = "NO" ]
then
clear
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
else
clear
echo "--------------------------------------------------------------------------"
echo " Oops! Invalid Entry ... Exiting out of the POCbot V1.0                   "
echo "--------------------------------------------------------------------------"
echo "Have a great Day! - Versa POC-BOT                                         "
echo "We always know automation is the future! We are always here to assist you!"
echo "--------------------------------------------------------------------------"
exit
fi
