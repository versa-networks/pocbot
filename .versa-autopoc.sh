#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.9
#  Last Modified : Sat May  2 14:01:48 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the VERSA POC-BOT Module

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

sh ./.header/.auto-poc-header.sh
echo "Enter Your choice: \c"
read template
        if [ "$template" = "1" ]
        then
                sh      ./.script/.autopoc/.autopoc.sh
        elif [ "$template" = "2" ]
        then
                sh ./.script/.autopoc/.autopocfilesession1.sh
        elif [ "$template" = "3" ]
        then
                sh ./.versa-auto-poc.sh
        elif [ "$template" = "exit" ]
        then
                clear
                echo "--------------------------------------------------------------------------"
                echo "Have a great Day! - Versa POC-BOT                                         "
                echo "We always know automation is the future! We are always here to assist you!"
                echo "--------------------------------------------------------------------------"
        else
                sh ./.versa-stealthmode.sh
        fi
