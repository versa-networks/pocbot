===================================================================
                    Versa Networks POCbotV1.0
-------------------------------------------------------------------
Introduction:
POCbot is a menu driven bot to assist with Versa Networks Secure 
SD-WAN POC VM infra implementations and beyond on Ubuntu 18.04 
based KVM hypervisors.
 
Getting Started:
These instructions will guide you through the steps to convert your 
Ubuntu 18.04 Server to a KVM based Hypervisor and get your POC VM 
Infra including and not limited to Versa Headend with Controller,
Director, Analytics, CPE, FlexVNF up and running.

Prerequisites:
Before installing POCbotV1.0, please check that the following 
conditions are met on your server,

• Processor - 32 core, 3 GHz (or equivalent) processor or better 
  with hardware virtualization features enabled. The system should 
  have either an Intel processor with the VT-x (vmx), or an AMD 
  processor with the AMD-V (svm) technology support. Enter the 
  following grep command to see if your processor supports hardware 
  virtualization:
  grep -Eoc '(vmx|svm)' /proc/cpuinfo
  If your CPU supports hardware virtualization, the command will 
  output a number greater than zero, which is the number of the CPU 
  cores. If the output is 0 it means that the CPU doesn’t support 
  hardware virtualization.
• Memory - minimum of 128GB of RAM is recommended
• Disk space – 2 TB or more
• Physical NIC - two or more network adapters
• Software – Ubuntu 18.04 LTS (Bionic Beaver) Server version 
  pre-installed and ssh configured
• You must have a copy of the zipped file v-POCbot-1.tgz copied to 
  your Ubuntu server to any desired folder
• POCbot support qcow2 image format [*.qcow2] ONLY. Tool has pre-
  checks to automatically detect the image format entered. Please 
  untar any qcow2.tbz2 image with ‘tar -xvf<image>.qcow2.tbz2’ 
  before calling in the script.
• Download a copy of the interested image of Director, Analytics, 
  Controller, CPE in any directory in the server
• POCbot must be run as a root user
• You will need an active internet connection during the initial 
  installation processes of the POCbot to install dependencies

Note: 
  The better the hardware, the bigger the topology you can build. 
  The number of Virtual CPUs that can be assigned to VMs is not 
  limited to the number of physical CPUs in the host. However, for 
  best results avoid over provisioning of the Server.

POCbot Modules: [Menu Driven Program]
# sh v-POCbot
├── 1 Preparing the Server
│   ├── 0 - Install PRE-REQUISITES packages               
│   ├── 1 - Configure Server Management and POC Bridges   
│   ├── 2 - Hardware/Software Autoscan                    
│   ├── 3 - Configure Versa POC VM's Management IP address
│   ├── 4 - Clean-up Virtual Machines [cherry-pick from list]
│   └── 5 - S.W.E.E.P   A.L.L   V.M   [Delete's ALL VM!]
└── 2 Build dynamic SDWAN POC VM Infra
    ├── 1 - User Interactive Infra Building Module   
    └── 2 - Building Infra Module from masterdata.txt

Installing and Deploying POC VM Infra using POCbot
++++++++++++++++++++++++++++++++++++++++++++++++++
Step 0: Untar the file v-POCbot-1.tgz and it will create a folder 
named v-poc,
      'tar -xvf v-POCbot-1.tgz'

root@versa:/home/versa/v-poc# ls -l
total 32
-rw-rw-r-- 1 versa versa   942 Sep 22 10:27 disclaimer.txt
-rw-r--r-- 1 root  root   3414 Oct 12 13:54 masterdata.txt
-rw-rw-r-- 1 versa versa 11174 Aug 26 20:12 README.txt
-rw-rw-r-- 1 versa versa  5237 Oct 11 20:04 sampledata.txt
-rwxrwxrwx 1 versa versa  1778 Aug 24 18:10 v-POCbot

Step 1: Change directory to folder v-poc and initiate the script 
v-POCbot as root user. v-POCbot is a shell script.
      sudo -i
      cd v-poc
      sh v-POCbot

Note: Run the POCbot from the extracted folder directly and not by
referring the path from any other remote folder. Script has inbuilt
intelligence to enforce it. 

Step 2: Prepare the server. Install the pre-requisite Packages 
needed for the POCbot. At the end of this step we would have 
converted the Vanilla Ubuntu server to a KVM based hypervisor. 

# sh v-POCbot
└── 1 Preparing the Server
    └── 0 - Install PRE-REQUISITES packages               

As part of this step, the script download package information from
all configured sources with 'apt-get -y update' and install the 
newest versions of all packages currently installed with 
'apt-get -y upgrade'.

Packages installed in this step are,
apt-get -f -y install qemu-kvm libvirt-bin virt-manager sshpass 
bridge-utils libvirt-clients virtinst inxi expect net-tools

Step 3: Configure Hypervisor Management bridge and POC Bridges. 
POCbot will auto create 30 persistent networking bridges with the 
name vbr101 to vbr130 on this step and a Management bridge based on 
the user data entered. The ip address for accessing the server will 
be configured on this Management bridge and all future VM’s eth0 
interface will be binded to this bridge for external access. POCbot 
will reboot the server automatically once post this config changes.

# sh v-POCbot
└── 1 Preparing the Server
    └── 1 - Configure Server Management and POC Bridges   

Script will look for these files and do the required config changes
  /etc/netplan/50-cloud-init.yaml
  /etc/netplan/01-netcfg.yaml
  /etc/netplan/01-network-manager-all.yaml
  /etc/netplan/00-installer-config.yaml

Step 4: Hypervisor Hardware and Software Pre-check. It is 
recommended to scan the underlying hypervisor hardware and software 
to confirm the server readiness to host Virtual Machines post 
package installation. This module shares information on hypervisor 
including but not limited to,
• Hardware in terms of the CPU/memory/ Network/Disk drives
• Software in terms of QEMU/KVM/libvirt
• List of Existing Virtual Machines up and the resources utilized
• Custom report on Versa Operating System Interface mapping

# sh v-POCbot
└── 1 Preparing the Server
    └── 2 - Hypervisor Hardware/Software Autoscan

Step 5: Initiate the dynamic POC Virtual Machine Infra with POCbot. 
There are two methods to build the SD-WAN Dynamic topology,
Method 1# User Interactive Mode
Method 2# From Static File Mode - masterdata.txt

# sh v-POCbot
└── 2 Build dynamic SDWAN POC VM Infra
    ├── 1 - User Interactive Infra Building Module   
    └── 2 - Building Infra Module from masterdata.txt
Copy the interested VOS qcow2 image to folder 
/var/lib/libvirt/images. This folder gets auto created post 
completing the step 2 [Preparing the Server]. Please untar any 
qcow2.tbz2 to qcow2 image with ‘tar -xvf <image>.qcow2.tbz2’ before 
calling in the script. This is a Fully user interactive interface 
to build the POC VM infra setup.

Method 1# User Interactive Mode

# sh v-POCbot
└── 2 Build dynamic SDWAN POC VM Infra
    └── 1 - User Interactive Infra Building Module   

POCbot gather user data in two parts [1/2] as below,
Part 1: Used to gather POC VM specification Information
        1.1: Versa Director specific details
        1.2: Versa Analytics specific details
        1.3: Versa Controller/CPE/Branches specific details
Part 2: Used to gather Bridge/Interface Information

Important Note:
# You can safely and completely avoid any sub-section [1.1/1.2/1.3]
  by not entering any data for that specific sub-section and by 
  pressing ENTER Key.
# For example, if you need only Director and Analytics in your 
  setup, you must input all data in sub-section 1.1 and 1.2 and you
  can completely avoid entering data in sub-section 1.3 by pressing
  ENTER key in that specific sub-section.
# Entering Partial data on any of the sub-section [1.1 /1.2 /1.3]
  intentionally or unintentionally will lead to completely neglect/
  ignore that specific sub-section by the POCbot.
# POCbotV1.0 supports VM images with qcow2 format [*.qcow2] ONLY.
  Tool has pre-checks to automatically detect the image format 
  entered. POCbot stops the tool execution with un-supported image 
  formats.
# It is recommended to copy the images to /var/lib/libvirt/images 
  folder and 
# POCbot will create new qcow2 image for individual Versa Operating
  Systems with the below naming nomenclature based on the User data
  entered on POCbot execution:
  User Entered VM name        : [VM name]
  User Entered Image location : [Image Path]+[Filename]
  POCbot Auto Created Image: [Image Path]+[VM name]+[Filename]

Part 1:

Please be handy with Versa Director, Analytics, Controller, CPE
resources specific details as below:

It is recommended to keep the images in /var/lib/libvirt/images 
folder for the ease of management
-Image FULL path: Image should be in qcow2 format ONLY [*.qcow2]
 Eg: /var/lib/libvirt/images/versa-flexvnf-80561de-16.1R2S9.qcow2
-VM names
-CPU counts
-Memory in Mb [Eg: For 8 GB Enter 8192]

Part 2:

Please be handy with Bridge related information of your Network
Versa Director, Analytics, Controller/CPE/Branches

 - Bridges will start with the naming terminology br<num>
 - <num> starts from 101 for the ease of management

Method 2# From Static File Mode - masterdata.txt
You can update the masterdata.txt file based on your topology 
and run the POCbot as below,

# sh v-POCbot
└── 2 Build dynamic SDWAN POC VM Infra
    └── 2 - Building Infra Module from masterdata.txt

This will create the topology as desired.

Step 6: Configure Management IP of the Versa virtual machines. 
POCbot will assist with management [eth0] interface configuration 
for the VM’s in the hypervisor. Initiate this tool only after 10 
minutes of Step 5 to give time for the Virtual Machines Operating 
systems to boot. This module will reboot the Virtual Machines post 
IP address configuration.

# sh v-POCbot
└── 1 Preparing the Server
    └── 3 - Configure Versa VM's Management[eth0] IP address

Managing Virtual Machines:
POCbot has modules to manage the VM’s in the hypervisor.
1 - Clean-up Virtual Machines [Read with caution:]
     This module is used to permanently delete the Virtual Machines 
configured in the Hypervisor/Bare Metal Server and its 
associated storage volumes. Multiple VM’s can be deleted in 
this module and are user driven data.
     Steps Carried out:
     i.  Force-stopping of interested virtual machines
     ii. Deleting interested Virtual Machines and its associated 
        storage volumes

# sh v-POCbot
└── 1 Preparing the Server
    └── 4 - Clean-up Virtual Machines

2 - S.W.E.E.P  A.L.L  V.M [Use with caution!]
     This Engine is used to permanently delete all the Virtual 
     Machines and the associated storage volumes in the Hypervisor  
     /Bare Metal Server.
     Steps Carried out:
     i.  Force-stopping of all virtual machines
     ii. Deleting all Virtual Machines and its associated storage 
         volumes

# sh v-POCbot
└── 1 Preparing the Server
    └── 5 - S.W.E.E.P   A.L.L   V.M
-------------------------------------------------------------------
Virtual Machine Image Handling 
++++++++++++++++++++++++++++++
The POCbot will create new qcow2 images with the nomenclature as 
below,
 User Entered Data on POCbot execution: 
 # VM name
 # Image location = [Image Path]+[Filename]
 POCbot Created Image naming nomenclature: 
 [Image Path]+[VM name]+[Filename]
  
 Eg: 
 VM name        : Director1
 Image location : versa-director-a454c1d-16.1R2S9.qcow2
 POCbot Created : Director1-versa-director-a454c1d-16.1R2S9.qcow2
 image
-------------------------------------------------------------------
Qualified Ubuntu Server Version Ubuntu 18.04
++++++++++++++++++++++++++++++++++++++++++++
root@versa:~# lsb_release -a
Distributor ID: Ubuntu
Description:    Ubuntu 18.04.4 LTS
Release:        18.04
Codename:       bionic
-------------------------------------------------------------------
Note: At any point of time, you may terminate the bot by pressing  
Ctrl+C
===================================================================
