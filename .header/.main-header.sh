#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.10
#  Last Modified : Tue Aug  4 17:49:41 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the Main Module 

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "____   _________________________  _________   _____                                   "
echo "\   \ /   /\_   _____/\______   \/   _____/  /  _  \                      _______     " 
echo " \   Y   /  |    __)_  |       _/\_____  \  /  /_\  \                    /[_]O|  \    "     
echo "  \     /   |        \ |    |   \/        \/    |    \                 _|_________|_  "
echo "   \___/   /_______  / |____|_  /_______  /\____|__  /                | |   ===   | | "
echo "                   \/         \/        \/         \/                  || [XX]O[#]||  "
echo "                __________________  ____________.           __         ||-'\"\"\"\"\"\"-||  "
echo "                \______   \_____  \ \_   ___ \_ |__   _____/  |_      _||_________||_ "
echo "                 |     ___//   |   \/    \  \/| __ \ /  _ \   __\     || \\_______/ || "
echo "                 |    |   /    |    \     \___| \_\ (  <_> )  |      /==\   /=\   /==\\"
echo "                 |____|   \_______  /\______  /___  /\____/|__| V1.0 [__]   [_]   [__]"
echo "                                  \/        \/    \/                                  "
echo " THE BOT to assist with Versa Networks Secure SD-WAN POC Implementations and beyond..." 
echo " -------------------------------------------------------------------------------------"
							 
echo " +----------------------------------------------+ "
echo " |  Modules:                                    | "
echo " +----------------------------------------------+ "
echo " |                                              | "
echo " |  1 - Preparing the Server                    | "
echo " |                                              | "
echo " |  2 - Build dynamic SDWAN POC VM Infra        | "
echo " |                                              | "
echo " |  Type 'exit' to exit program                 | "
echo " |                                              | "
echo " +----------------------------------------------+ "
