#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.10
#  Last Modified : Tue Aug  4 17:49:41 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the Main Module 

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH 
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE 
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR 
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN 
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "+-----------------------------------------------------------+"
echo "|___  ____ ____ ___  ____ ____ _ _  _ ____                  |"
echo "||__] |__/ |___ |__] |__| |__/ | |\ | | __                  |"
echo "||    |  \ |___ |    |  | |  \ | | \| |__]                  |"
echo "|            ___ _  _  ___     ___  ___  ___ _  _  ___  ___ |" 
echo "|             |  |__| |___    [__  |___ |__/ |  | |___ |__/ |" 											 
echo "|             |  |  | |___    ___] |___ |  \  \/  |___ |  \ |" 
echo "|                                                           |"
echo "|   0 - Install PRE-REQUISITES packages                     |"
echo "|                                                           |"
echo "|   1 - Configure Hypervisor Management and POC Bridges     |"
echo "|                                                           |"
echo "|   2 - Hypervisor Hardware/Software Autoscan               |"
echo "|                                                           |"
echo "|   3 - Configure VOS Management[eth0] IP address           |"
echo "|                                                           |"
echo "|   4 - Clean-up Virtual Machines [cherry-pick from list]   |"
echo "|                                                           |"
echo "|   5 - S.W.E.E.P   A.L.L   V.M   [Delete's ALL VM!]        |"
echo "|                                                           |"
echo "|   6 - Go back to the Main Page                            |"
echo "|                                                           |"
echo "|   Type 'exit' to exit program                             |"
echo "+-----------------------------------------------------------+"
