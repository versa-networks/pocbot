#!/bin/sh
# +------------------------------------------------------+
#  Author        : rengaramalingam@versa-networks.com
#  Version       : 1.10
#  Last Modified : Wed Sep 23 15:41:50 UTC 2020
#  Hypervisor    : Tested on Ubuntu 18.04.3 LTS
# +------------------------------------------------------+
# This is the Header for the Main Module

# IT IS SOLELY BUILT TO ASSIST WITH VERSA NETWORKS SDWAN POC IMPLEMENTATION ON KVM HYPERVISOR
#
# PLEASE NOTE THAT THIS TOOL/SCRIPT HAS NO WARRANTY AND NO SUPPORT OBLIGATIONS ASSOCIATED WITH
# IT. THIS TOOL/SCRIPT IS NOT OFFICIALLY SUPPORTED AND THE USER ASSUMES ALL LIABILITY FOR THE
# USE OF THIS TOOL AND ANY SUBSEQUENT LOSSES
#
# IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR
# CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
# WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
# CONNECTION WITH THE USE OR PERFORMANCE OF THIS TOOL/SCRIPT.

clear
echo "+-----------------------------------------------------------+"
echo "|___  _  _ _ _    ___     ___  _   _ _  _ ____ _  _ _ ____  |"
echo "||__] |  | | |    |  \    |  \  \_/  |\ | |__| |\/| | |     |"
echo "||__] |__| | |___ |__/    |__/   |   | \| |  | |  | | |___  |"
echo "|        ____ ___  _ _ _ ____ _  _    _ _  _ ____ ____ ____ |"
echo "|        [__  |  \ | | | |__| |\ |    | |\ | |___ |__/ |__| |"
echo "|        ___] |__/ |_|_| |  | | \|    | | \| |    |  \ |  | |"
echo "|                                                           |"
echo "|                                                           |"
echo "|   1 - User Interactive Infra Building Module              |"
echo "|                                                           |"
echo "|   2 - Building Infra Module from masterdata.txt           |"
echo "|                                                           |"
echo "|   3 - Go back to the Main Page                            |"
echo "|                                                           |"
echo "|   Type 'exit' to exit program                             |"
echo "+-----------------------------------------------------------+"
