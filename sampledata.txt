===================================================================
Note:
# Please don’t change the alignment/content of the file
# This is the sample file ONLY for reference
# Please fill in the actual data in masterdata.txt file
# Use the right half-plane to enter user input variables, after ":"
# There should be a space always after ":" before entering any data
# Leave the variables field empty after ":" which are not needed
  in your topology
# space character is not supported in instance names variable
# Full Image Path should be in the below format.
  Eg:/var/lib/libvirt/images/versa-director-3c2e1c6-21.1.1.qcow2
# qcow2 format only is supported as valid input
===================================================================
Sample Topology:

Please view in a fixed-width font such as Courier.

                                        +----+
          +--------------+--------------+    |
          |eth0          |eth0     +--+ |    |
     +----+----+     +---+------+  |S | |    |
     |Director1|     |Analytics1|  |e | |    |
     +------+--+     +--+-------+  |a | |mgmtbridge
            |eth1       |eth1      |r | |    |
           ++-----------++     eth1|c +-+    |
           |    vbr103   +---------+h | |    |
           +------+------+         |1 | |    |
                  |                +--+ |    |
            +-----+------+ eth0         |    |
            |Controller1 +--------------+    |
        +---+            +---+          |    |
        |   +------------+   |          |    |
+-------+-----+       +------+------+   |    |
|    vbr101   |       |   vbr102    |   |    |
+---+---------+       +------------++   |    |
    | +--------------+-------------+    |    |
    +-)-----------+--)----------+  |    |    |
    | |           |  |          |  |    |    |
+---+-+-+     +---+--++     +---+--++   |    |
| BS1   |     |  BS2  |     | BS3   |   |    |
|       |     |       |     |       |   |    |
+---+---+     +---+---+     +---+---+   |    |
    |eth0         |eth0         |eth0   |    |
    +-------------+-------------+-------+    |
                                        +----+

===================================================================
Please Fill in Versa Director Instances Details
Versa Director Full Image Path                 : /var/lib/libvirt/images/versa-director-3c2e1c6-21.1.1.qcow2
Versa Director instance names                  : Director1
[names seperated by space]
Versa Director CPU counts                      : 6
Versa Director Memory in Mb                    : 8192

Please Fill in Versa Analytics Instances Details
Versa Analytics Full Image Path                : /var/lib/libvirt/images/versa-analytics-6e4e455-21.1.1.qcow2
Versa Analytics instance names                 : Analytics1 Search1
[names seperated by space]
Versa Analytics CPU counts                     : 6
Versa Analytics Memory in Mb                   : 8192

Please Fill in Versa Controller/CPE/Branches Instances Details
Versa Controller Full Image Path               : /var/lib/libvirt/images/versa-flexvnf-6e4e455-21.1.1.qcow2
Versa Controller instance names                : Controller1
[names seperated by space]
Versa Controller CPU counts                    : 6
Versa Controller Memory in Mb                  : 6144
Versa CPE-Branches instance names              : BS1 BS2 BS3
[names seperated by space]
Versa CPE-Branches CPU counts                  : 4
Versa CPE-Branches Memory in Mb                : 4096

Please Fill in topology related information
Existing Management Bridge Name                : mgmtbridge
Instance Names to be part of Management Bridge : Director1 Analytics1 Search1 Controller1 BS1 BS2 BS3
Instance Names to be part of BRIDGE-vbr101     : Controller1 BS1 BS2 BS3
Instance Names to be part of BRIDGE-vbr102     : Controller1 BS1 BS2 BS3
Instance Names to be part of BRIDGE-vbr103     : Director1 Analytics1 Search1 Controller1
Instance Names to be part of BRIDGE-vbr104     :
Instance Names to be part of BRIDGE-vbr105     :
Instance Names to be part of BRIDGE-vbr106     :
Instance Names to be part of BRIDGE-vbr107     :
Instance Names to be part of BRIDGE-vbr108     :
Instance Names to be part of BRIDGE-vbr109     :
Instance Names to be part of BRIDGE-vbr110     :
Instance Names to be part of BRIDGE-vbr111     :
Instance Names to be part of BRIDGE-vbr112     :
Instance Names to be part of BRIDGE-vbr113     :
Instance Names to be part of BRIDGE-vbr114     :
Instance Names to be part of BRIDGE-vbr115     :
Instance Names to be part of BRIDGE-vbr116     :
Instance Names to be part of BRIDGE-vbr117     :
Instance Names to be part of BRIDGE-vbr118     :
Instance Names to be part of BRIDGE-vbr119     :
Instance Names to be part of BRIDGE-vbr120     :
Instance Names to be part of BRIDGE-vbr121     :
Instance Names to be part of BRIDGE-vbr122     :
Instance Names to be part of BRIDGE-vbr123     :
Instance Names to be part of BRIDGE-vbr124     :
Instance Names to be part of BRIDGE-vbr125     :
Instance Names to be part of BRIDGE-vbr126     :
Instance Names to be part of BRIDGE-vbr127     :
Instance Names to be part of BRIDGE-vbr128     :
Instance Names to be part of BRIDGE-vbr129     :
Instance Names to be part of BRIDGE-vbr130     :
